<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/metro.css">
        <link rel="stylesheet" href="css/metro-icons.css">
        <link rel="stylesheet" href="css/estilos.css">
        <script src="js/jquery-2.1.3.min.js"></script>
        <script src="js/metro.js"></script>
        <script src="js/jquery.corner.js"></script>
        <script src="js/inicio.js"></script>
        
        <title></title>
    </head>
    
        
    
        <div class="login-form center center-inner">
            <form   id="formulario" action="#" method="post">
            
            <div class="input-control text full-size" data-role="input">
                <label for="usuario_nombre">Usuario:</label>
                <span class="mif-user prepend-icon" style="color:#D02921"></span>
                <input type="text" name="usuario_nombre" id="usuario_nombre" required placeholder="Usuario" >
                <button class="button helper-button clear"><span class="mif-cross"></span></button>
            </div>
            <br />
            <br />
            <div class="input-control password full-size" data-role="input">
                <label for="usuario_password">Password</label>
                 <span class="mif-lock prepend-icon"></span>
                <input type="password" name="usuario_password" id="usuario_password" required placeholder="Password">
                <button class="button helper-button reveal"><span class="mif-looks"></span></button>
            </div>
            <br />
            <br />
            <div class="form-actions">
                <button type="submit" class="button bg-grayDark bg-active-grayDark fg-white">Iniciar sesión</button>
                <button type="button" class="button bg-grayDark bg-active-grayDark fg-white" onclick="showDialog('#dialog');">Registrar</button>
            </div>
        </form>
        </div>
        
     <div data-role="dialog" data-close-button="true"  data-background="dialogoBackground" data-overlay-color="op-dark" data-overlay="true" id="dialog" >
        <h1>Registrar</h1>
         <hr class="thin"/>
         <div class="container">
             <form id="formRegistro" action="#"  method="post">
           <div class="input-control text full-size" data-role="input">
                <label for="usuario_nuevo">Usuario:</label>
                <span class="mif-user prepend-icon" style="color:#D02921"></span>
                <input type="text" name="usuario_nuevo" id="usuario_nuevo" required placeholder="Usuario" >
                <button class="button helper-button clear"><span class="mif-cross"></span></button>
            </div>
            <br />
            <br />
            <div class="input-control password full-size" data-role="input">
                <label for="usuario_nuevo_password">Password</label>
                 <span class="mif-lock prepend-icon"></span>
                <input type="password" name="usuario_nuevo_password" id="usuario_nuevo_password" required placeholder="Password">
                <button class="button helper-button reveal"><span class="mif-looks"></span></button>
            </div>
            <br />
            <br />
            <div class="input-control password full-size" data-role="input">
                <label for="usuario_nuevo_confirm">Confirmar Password</label>
                 <span class="mif-lock prepend-icon"></span>
                <input type="password" name="usuario_nuevo_confirm" id="usuario_nuevo_confirm" required placeholder="Password">
                <button class="button helper-button reveal"><span class="mif-looks"></span></button>
            </div>
            
            <div class="form-actions">
                <button type="submit" class="button bg-grayDark bg-active-grayDark fg-white">Registrar</button>
            </div>
            
        </form>
    </div>
        
    </div>
       
    
</html>
