<?php

class Usuario
                {
                    
                    private $usuario;
                    private $password;
                    private $conn;
                    public function __construct($data)
                    {
                      
                        if(isset($data['usuario_nombre']) && isset($data['usuario_password'])){
                            $this->usuario=$data['usuario_nombre'];
                            $this->password=$data['usuario_password'];
                        }
                        try{
                         //tratamos de abir la conexión
                         $this->conn= new PDO('mysql:host=localhost;dbname=hotel',"root","1234");
                         $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
                        }
                        catch(PDOException $ex)
                            {
                                echo $ex->getMessage();
                                return false;
                            }
                    }
                    public function _construct()
                    {
                        
                    }
                    public  function conectar()
                    {
                        try{
                          
                          $query=$this->conn->prepare("SELECT*FROM USUARIO WHERE PASSWORD=:password");
                          $query->bindParam(":password", $this->password);
                          $query->execute();
                
                          if($query->rowCount()>0)
                          {
                                //inicia la sesión con el usuario que fue encontrado en la base de datos
                                session_start();
                                $_SESSION["usuario"]=$this->usuario;
                                $_SESSION["password"]=$this->password;
                                return true;
                              
                          }
                        
                        }
                        //el usuario no fue encontrado en la BD
                        catch(PDOException $e)
                        {
                            echo $e->getMessage();
                            return false;
                        }
                    }
     
                    public function crearUsuario($nuevoUsuario,$nuevaPassword)
                    {
        
                            try{
                            
                                $query=$this->conn->prepare("INSERT INTO USUARIO(NOMBRE,PASSWORD) VALUES(:nombre,:password)");
                                $query->bindParam(":nombre", $nuevoUsuario);
                                $query->bindParam(":password", $nuevaPassword);
                                $query->execute();
                                return true;
                                
                             }
                            catch(PDOException $ex)
                            {
                                echo $ex->getMessage();
                                return false;
                            }
                       
                    }
                    
                }
                